import { Storage } from "@ionic/storage";

import { NotesService } from './notes.service';
import { Note } from '../interfaces/notes';

describe('NotesService', () => {
  let notesService: NotesService;

  beforeEach(() => {
    const configStorage = {
      // Define Storage
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    };
    const storage = new Storage(configStorage);
    notesService = new NotesService(storage);
  });

  afterEach(()=> {});

  it('should be created', () => {
    let notes: Note[];
    notes = notesService.notes;

    expect(notesService).toBeTruthy();
  });

  it('should be empty', () => {
    let notes: Note[];
    notes = notesService.notes;

    expect(Array.isArray(notes)).toBeTruthy();
    expect(notes.length).toEqual(0);
  });

  it('should have one note', () => {
    let notes: Note[];
    notes = notesService.notes;

    notesService.createNote("1st Note");
    expect(notes.length).toEqual(1);
  });

  it('should have two notes', () => {
    let notes: Note[];
    notes = notesService.notes;

    notesService.createNote("1st Note");
    notesService.createNote("2nd Note");

    expect(notes.length).toEqual(2);
  });

  it('should have one note then be empty', () => {
    let notes: Note[];
    notes = notesService.notes;

    let note = notesService.createNote("1st Note");
    expect(notes.length).toEqual(1);
    notesService.deleteNote(note);
    expect(notes.length).toEqual(0);

  });
});
